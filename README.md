# xtream-dev

Depenedency maintainer for building xtream from source. It keeps track of xtream:s submodule dependencies on gtest and std_utils.

```mermaid
graph TD;
  xtream-dev-->xtream;
  xtream-dev-->gtest;
  xtream-dev-->std_utils;
```

# Installation #
```sh
git clone git@gitlab.com:jipe4153/xtream-dev.git
cd xtream-dev
# Install (will initialize the submodules)
./install.sh
```

# Building (tests and samples) #

For running tests and sample codes.

```sh
cd xtream-dev
# update submodules
git submodule update --init
# Run cmake
cmake .
# Run make
make -j11
```
