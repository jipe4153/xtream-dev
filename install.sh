#!/bin/bash
set -e
#
# update submodules
git submodule update --init
# Run cmake
cmake .
make -j11
sudo make install
echo "Use find_package(xtream REQUIRED) in your CMakeLists.txt to find installed xtream"
